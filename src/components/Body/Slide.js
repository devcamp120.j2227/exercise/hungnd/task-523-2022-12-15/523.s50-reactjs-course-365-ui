import { Button, Grid, Stack } from "@mui/material";
import { Container } from "@mui/system";
import slideImg from "../../assets/images/ionic-207965.png";
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
    palette: {
      primary: {
        main: "#fff",
        contrastText: "#2c3e50",
      },
      secondary: {
        main: '#e67e22',
        contrastText: "white",
      },
    },
  });
const BodySlide = () => {
    return(
        <div className="slide-body">
            <Container>
                    <Grid container alignItems="center">
                        <Grid item xs={6} textAlign="left">
                            <h1>
                                Welcome to Ionic Course365
                            </h1>
                            <h1>
                                Learning Center
                            </h1>
                            <p>Ionic Course365 is an online learning and teaching marketplace with over 5000 courses
                                and 1 million students. Instructor and expertly crafted courses, designed for the mordern students and entrepreneur
                            </p>
                            <Stack direction="row" spacing={2} marginTop={8}>
                            <ThemeProvider theme={theme}>
                                <Button color="secondary" variant="contained">Browse Course</Button>
                                <Button color="primary" variant="contained" >Become An Intructor</Button>
                            </ThemeProvider>
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <img src={slideImg}/>
                        </Grid>
                
                    </Grid>
            </Container>
        </div>
    )
}
export default BodySlide;