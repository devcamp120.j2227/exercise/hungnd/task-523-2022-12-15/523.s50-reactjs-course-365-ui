import { Container } from "@mui/system";
import React, { useContext } from "react";
import {CourseContext} from "../Body";
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import {Grid, Card, CardHeader, CardContent, CardMedia, Avatar, IconButton, Typography, CardActions} from "@mui/material";
const RecommendCourse = () =>{
    const valueFromContext = useContext(CourseContext);
    return(
        <Container>
            <h2>Recommend to you</h2>
            <Grid container m={2} spacing={1}>  
                { valueFromContext.slice(0,4).map((value, index) =>{
                return (
                <Grid item xs={3} className="courses-365" key={index}>
                        <Card>
                            {
                                <CardMedia
                                component="img"
                                height="140"
                                src={require("../../../assets/"+ value.coverImage)}
                                alt="Nicola Sturgeon on a TED talk stage"
                                />
                            }
                            <CardContent>
                                {
                                <Typography variant="body">
                                    <h3> {value.courseName}</h3>
                                    <Grid
                                    container
                                    direction="row"
                                    justifyContent="start"
                                    alignItems="center"
                                    ><AccessTimeIcon/> 
                                    <p> {value.duration} {value.level} </p>
                                    </Grid>
                                    <Grid
                                        container
                                        direction="row"
                                        justifyContent="flex-start"
                                        alignItems="center">
                                        
                                        <p id ="price-normal"> ${value.price}</p><p id ="price-discount">${value.discountPrice}</p>
                                    </Grid>
                                </Typography>
                                }
                            </CardContent>
                            <CardHeader className="footer-card"
                                avatar={
                                <Avatar
                                alt="Teacher Photo"
                                src={require("../../../assets/"+ value.teacherPhoto)}
                                />
                                }
                                subheader={
                                <CardActions>
                                    <Grid
                                    container
                                    direction="row"
                                    justifyContent="start"
                                    alignItems="center"
                                    >
                                    <p>{value.teacherName}</p>
                                    </Grid>
                                    <IconButton>
                                        <BookmarkBorderIcon />
                                    </IconButton>
                                </CardActions>
                            }/>
                        </Card>
                </Grid>)})}
            </Grid>
        </Container>
    )
}
export default RecommendCourse;