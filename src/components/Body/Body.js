import { Container } from "@mui/system";
import React, { useState, useEffect, createContext, useContext } from "react";
import PopularCourse from "./Courses/popularCourse";
import RecommendCourse from "./Courses/recommendCourse";
import TrendingCourse from "./Courses/trendingCourse";
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    return data;
}
export const CourseContext = createContext();
const Body = () =>{
    const [Product, showProduct] = useState([]);
    useEffect(() => {
        fetchApi("http://localhost:8000/api/courses")
    .then((data) =>{
        showProduct(data.data);
    })
    .catch((error)=>{
        console.log(error.message)
    })
      }, []);

      return (
        <Container>
                <CourseContext.Provider value={Product}>
                    <RecommendCourse/>
                    <PopularCourse/>
                    <TrendingCourse/>
                </CourseContext.Provider>
        </Container>
    )
}
export default Body;